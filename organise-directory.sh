
# write your code in Bash
#!/bin/bash

folders='images music programs videos log'
ext_img="*.jpg *.JPG *.png *.gif"
ext_music="*.mp3 *.aac *.wma *.flac"
ext_vid="*.mp4 *.mkv *.flv *.avi *.webm *.wmv *.mov"
ext_log="*.log"

for folder in $folders
do 

	if [ $folder == 'images' ]
	then
		mkdir -p $folder
		mv $ext_img $folder 2>/dev/null

	elif [ $folder == 'music' ]
	then
		mkdir -p $folder
		mv $ext_music $folder 2>/dev/null
	
	elif [ $folder == 'videos' ]
	then
		mkdir -p $folder
		mv $ext_vid $folder 2>/dev/null

    elif [ $folder == 'log' ]
	then
		mkdir -p $folder
		mv $ext_log $folder 2>/dev/null
        rm -rf $folder

	else
		echo "File extension does not matches and will be unchanged !!Problem creating folder"
	fi

done

echo "Successfully organized"


