# Organise-directory-bash

This bash script organises the directory in multiple other directories based on the file type. It also removes the log files present in parent directory, rest all other files are unimpacted.

## Input Required

movie.jpg
test2.jpg
test.txt
netflix/test.log
audit.log
pic.falc
test3.mov

## Script Output

image:
- movie.jpg
- test2.jpg
netflix/test.log
movie:
- pic.falc
- test3.mov
